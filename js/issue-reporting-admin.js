;(function ($) {

  $(window).load(function(){
    //Define elements
    var $iraToolbar = $('#ir-toolbar-admin');
    var $iraToolbarMessage = $('#ir-toolbar-admin-message .content');
    var $iraToolbarShow = $('#ir-toolbar-admin-show');
    var $iraToolbarHide = $('#ir-toolbar-admin-hide');
    var $iraToolbarPageIssues = $('#ir-toolbar-admin-page-issues');

    var iraToolbarOn = false;

    //send the AJAX get from the server
    var webserver = $iraToolbar.data('webserver') + '/issue-reporting/list';
    var data = {
      path : window.location.pathname + window.location.hash + window.location.search,
    };

    if ($iraToolbar.data('project') != ''){
      data.project = $iraToolbar.data('project');
    }

    if ($iraToolbar.data('user') != undefined){
      data.user = $iraToolbar.data('user');
    }

    $.get(webserver, data, function(data){
      //check if success
      if (data == "no results") {return '';}

      //store a lookup array of all comments by element
      var iraIssues = new Array();

      $.each(data, function(i, issue){

        //build out the lookup array
        if (iraIssues[issue.element] == undefined){
          iraIssues[issue.element] = new Array();
        }

        iraIssues[issue.element].push(issue);

        //add click actions of all reported elements
        if (issue.element != 'none'){
          $(issue.element)
            .addClass('has-issue')
            .click(function(e){

              if (iraToolbarOn == false) {return;}

              //Stop clicks on links
              e.preventDefault();
              
              //Stop all further handlers
              e.stopImmediatePropagation();

              var message = '';

              $.each(iraIssues[issue.element], function(j, luIssue){
                message += '<p>' + luIssue.date_string + '<br>' + luIssue.comment + '</p>';
              });

              $iraToolbarMessage.html(message);

            });
        }
      });

      //define toolbar buttons
      $iraToolbarShow.click(function(){
        $iraToolbarHide.addClass('active');
        $iraToolbarShow.removeClass('active');
        $iraToolbarMessage.addClass('active');
        $iraToolbarPageIssues.addClass('active');

        iraToolbarOn = true;
        $('body').addClass('show-issues');
      });

      $iraToolbarHide.click(function(){
        $iraToolbarShow.addClass('active');
        $iraToolbarHide.removeClass('active');
        $iraToolbarMessage.removeClass('active');
        $iraToolbarPageIssues.removeClass('active');

        iraToolbarOn = false;
        $('body').removeClass('show-issues');
      });

      $iraToolbarPageIssues.click(function(){
        var message = '';

        $.each(iraIssues['none'], function(j, luIssue){
          message += '<p>' + luIssue.date_string + '<br>' + luIssue.comment + '</p>';
        });

        $iraToolbarMessage.html(message);
      });

    });

  }); //end of document ready

})(jQuery);
