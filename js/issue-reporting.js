;(function ($) {

  $(window).load(function(){

    //Define elements
    var $irToolbar = $('#ir-toolbar');
    var $irToolbarMessage = $('#ir-toolbar-message .content');
    var $irToolbarForm = $('#ir-toolbar-form');
    var $irToolbarActions = $('#ir-toolbar-actions');

    //Define settings
    var selectElementOn = false;
    var reportedElement;
    var reportedMessage;

    //Report Element
    $('#ir-toolbar-report-element').click(function(e){
      //Start watching for selection of element
      selectElementOn = true; 

      $irToolbarActions.removeClass('active');
      $irToolbarMessage.html("Click on a part of the page or button that you have an issue with.");
    });

    $('*').click(function(e){
      var clickedElement = e.srcElement;
      //Save the jQuery object
      var $clickedElement = $(clickedElement);

      //We check if reporting is on and the clicked element is not the toolbar or the main container
      if (selectElementOn === true && $clickedElement.parents('#ir-toolbar').length == 0 && $clickedElement.attr('id') != 'main'){
        
        //Stop clicks on links
        e.preventDefault();
        
        //Stop all further handlers
        e.stopImmediatePropagation();

        //Stop watching for selection of element
        selectElementOn = false; 

        //We need to get some type of identifier for the element so we can reference it with jQuery later.
        //We are going to build a selector out of Tag, ID, Class, HREF, and TITLE.
        //We then check if this selector can be used as a unique identifier on that page. If not, we build a more detailed selector using unique parents.
        
        var $reportedElement = $clickedElement;

        function buildSelector(){
          var selector = $reportedElement[0].tagName.toLowerCase();
          
          if ($reportedElement.attr('id') != ''){selector += '#' + $reportedElement.attr('id');}
          if (!($.inArray($reportedElement.attr('class'), ['', 'has-issue']))){
            selector += '.' + $reportedElement.attr('class').split(' ').join('.');
            //remove 'has-issue class'
            selector = selector.replace('.has-issue', '');
          }
          if ($reportedElement.attr('href') != undefined){selector += '[href="' + $reportedElement.attr('href') + '"]';}
          if ($reportedElement.attr('title') != ''){selector += '[title="' + $reportedElement.attr('title') + '"]';}
          
          return selector;
        }

        reportedElement = buildSelector();

        //We check if the selector returns only a single element on the page
        while ($(reportedElement).length != 1) {
          //We move up the DOM to see if the parent element can be used to build a more unique selector
          $reportedElement = $reportedElement.parent();

          //We build out the selector by prepending the parent selector
          reportedElement = buildSelector() + ' > ' + reportedElement;
        }

        //Move on to the form
        $irToolbarForm.addClass('active');
        $irToolbarMessage.html("Please leave a comment and submit the form");
      }
    });

    //Report Page
    $('#ir-toolbar-report-other').click(function(e){
      reportedElement = 'none';
      $irToolbarActions.removeClass('active');
      $irToolbarForm.addClass('active');
      $irToolbarMessage.html("Click on a part of the page or button that you have an issue with.");
    });

    $('#ir-submit-feedback').click(function(){

      //send the AJAX post to the server
      var webserver = $irToolbar.data('webserver') + '/issue-reporting/add';
      var data = {
        comment : $('#ir-toolbar-comment').val(),
        element : reportedElement,
        path : window.location.pathname + window.location.hash + window.location.search,
        project : $irToolbar.data('project'),
        user : $irToolbar.data('user'),
      };

      $.post(webserver, data, function(){
        $irToolbarForm.removeClass('active');
        $irToolbarActions.addClass('active');
        $irToolbarMessage.html("You have reported an issue.");
        //clear the comment
        $('#ir-toolbar-comment').val('');
      });

    });

  }); //end of document ready

})(jQuery);
